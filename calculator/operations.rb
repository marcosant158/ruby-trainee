require_relative '../extra_operations'
require 'net/http'
require 'json'

module Calculator
  class Operations
    include ExtraOperations
      
    def biased_mean(grades, blacklist)
    grades = JSON.parse(grades) #json padrão pra transformar string em hash
    soma = 0 #soma tem que ter um valor previo zerado
      alunos = 0 #alunos tem que ter um valor previo zerado
    grades.each do |nome,nota| #forma como vamos colocar os nomes e as notas pro gets.chomp pegar
    
    if (not blacklist.include?(nome)) #quem tirar com a blacklist
      soma += nota #soma das notas
      alunos += 1 #soma dos alunos
    end
  end
  return(soma.to_f/alunos.to_f).to_f #média soma/quantidade de alunos
end
    def no_integers(numbers)
      numbers = numbers.split(" ")
      division = ["00", "25", "50","75"]
      result = ""
      numbers.each do |numero|
      if(division.include?(numero[-2..-1]))
        result +="S "
      else
        result +="N "
    end
  end
  return result
end
    def filter_films(genres, year)
      genres = genres.split(" ")
      films = get_films[:movies]
      resultado = []
      films.each do |coisa|
       if  (coisa[:year] >= year && (genres & coisa[:genres]) == genres )
        resultado.push(coisa[:title])
       end  
      end
      return resultado
    end
    
    private
  
    def get_films
      url = 'https://raw.githubusercontent.com/yegor-sytnyk/movies-list/master/db.json'
      uri = URI(url)
      response = Net::HTTP.get(uri)
      return JSON.parse(response, symbolize_names: true)
    end
  end
end
