require_relative 'operations'

module Calculator
  class Menu
    def initialize
      operations = Operations.new
      puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
      puts "bem vindo a calculadora especial"
      puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
      puts "escolha uma das funções abaixo:"
      puts "1- Média preconceituosa."
      puts "2- Calculadora sem números"
      puts "3- Filtrar filmes"
      puts "0- Sair"

      escolha = gets.chomp.to_i
      case escolha
        when 1
        puts "Bem vindo à Média preconceituosa"
        puts "Insira os nomes e notas dos alunos: "
        names = gets.chomp

        puts "Insira o nome dos alunos que não vão entrar na média:"
        blacklist = gets.chomp
        puts "A média será de:"
        answer = operations.biased_mean(names, blacklist)
        puts "#{answer}"
        when 2
          puts "bem vindo à calculadora sem números"
          puts "Insira os números que quer derscobrir se são divisíveis por 25:"
          numbers = gets.chomp
          puts"Os números com S são divisíveios, com N, não são: "
          resultado = operations.no_integers(numbers)
          puts" #{resultado}"
        when 3
          puts"Digite os Gêneros"
          genres = gets.chomp
          puts"Digite o ano"
          year = gets.chomp
          puts operations.filter_films(genres, year)       
        when 0
          puts"Até a próxima."
          exit
        end
    end
  end
end
